﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace MatrixMul
{
    ///<summary>
    /// Calculates a product of two matrices using iterative algorithm.
    /// 
    /// MatrixMul [m][n][p]
    /// 
    /// 1. If no arguments passed - Multiplying A(100x100) * B(100*100)
    /// 2. If one argument is passed - multiplying A(m x m) * B(m x m)
    /// 3. If two arguments are passed - multiplying A(m x n ) * B(m x n)
    /// 4. If three arguments are passed - multiplying A(m x n) * B(n x p)
    /// 5. If more than tree arguments are passed, only first trhee are used and the rest are ignored.
    /// 
    /// Same calculation is performed on the same matrices:
    /// 1.  Represented as 2D array - takes 19.35 seconds on my computer
    /// 2.  Represented as an Array-of-arrays - takes 17.65 seconds
    /// 
    /// Surprisingly, Matrices represented as Arrays-of-arrays, were multiplied faster than the regular 2d matrices.
    /// </summary>   
    class Program
    {
        /// <summary>
        /// Array dimensions:
        /// A(m1, n1), B(m2, n2)
        /// </summary>
        public static int m,n,p;
        
        static void ReadArgs(string[] args)
        {
            int[] dimensions = new int[4];
            for (int i = 0; (i < args.Length) && (i < 4); i++)
            {                
                    dimensions[i] = int.Parse(args[i]);
            }
            if (args.Length == 0)
            {
                m = n = p = 100;
            }
            else if (args.Length == 1)
            {
                m =  n = p = int.Parse(args[0]);
            }
            else if (args.Length == 2)
            {
                m = n = int.Parse(args[0]);
                p = int.Parse(args[1]);
            }
            else if (args.Length > 2)
            {
                m = int.Parse(args[0]);
                n = int.Parse(args[1]);
                p = int.Parse(args[2]);                
            }
        }

        static void Main(string[] args)
        {
            if (args.Length > 3)
            {
                Console.WriteLine("Only the first 4 arguments will be used, the rest are ignored.");
            }
            try
            {
                ReadArgs(args);
            }
            catch
            {
                Console.WriteLine("Wrong input arguments");
            }

            Console.WriteLine("Multiplying matrices:  A({0}x{1}) B({2}x{3})", m, n, n, p);

            // 2D Arrays                                                 
            double[,] A1 = new double[m, n];
            double[,] B1 = new double[n, p];

            // Arrays-of-Arrays
            double[][] A2 = new double[m][];           
            double[][] B2 = new double[n][];

            // Resulting matrix
            double[,] C = new double[n, p];

            // Creating inner Arrays
            for (int i = 0; i < m; i++)
            {
                A2[i] = new double[n];
            }
            for (int i = 0; i < n; i++)
            {
                B2[i] = new double[p];
            }

            Random randomObj = new Random();
            // Initializing matrix A with random numbers
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    A1[i, j] =randomObj.NextDouble();
                    A2[i][j] = A1[i, j];
                }
            }

            // Initializing matrix B with random numbers
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    B1[i, j] = randomObj.NextDouble();
                    B2[i][j] = B1[i,j];
                }
            }
            
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            // Multiplying matrices as 2D Arrays 
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < p; j++)
                {
                    double sum = 0;
                    for (int k = 0; k < m; k++)
                    {
                        sum += A1[i, k] * B1[k, j];
                    }
                    C[i, j] = sum;
                }
            }

            stopWatch.Stop();
            long time2D = stopWatch.ElapsedMilliseconds;
            stopWatch.Reset();

            stopWatch.Start();
            // Multiplying matrices as Arrays-of-Arrays
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < p; j++)
                {
                    double sum = 0;
                    for (int k = 0; k < m; k++)
                    {
                        sum += A2[i][k] * B2[k][j];
                    }
                    C[i, j] = sum;
                }
            }

            stopWatch.Stop();
            long timeAoA = stopWatch.ElapsedMilliseconds;
            long ops = m * n * p * 2;

            Console.WriteLine("\nMultiplication time");
            Console.WriteLine("\t 1. 2D arrays {0}ms, {1} GFLOPS",time2D, 1e-9 * ops/time2D);
            Console.WriteLine("\t 2. Arrays-of-arrays {0}ms {1} GFLOPS ", timeAoA, 1e-9 * ops /timeAoA);
            Console.ReadKey();
        }
    }
}